/**
 * @file
 * JavaScript for prepopulation of field_office in node 'Edit' form.
 */

(function ($) {

  var district_selected = $('#edit-field-district').val();
  var office_selected = $('#edit-field-office').val();

  var html = '<option value="_none">- None -</option>';

  switch (district_selected) {
    case 'district_1':
      html += '<option value="office_1_district_1">Office 1 (district 1)</option>';
      html += '<option value="office_2_district_1">Office 2 (district 1)</option>';
    break;

    case 'district_2':
      html += '<option value="office_1_district_2">Office 1 (district 2)</option>';
      html += '<option value="office_2_district_2">Office 2 (district 2)</option>';
    break;

    default:
      html += '<option value="office_1_district_1">Office 1 (district 1)</option>';
      html += '<option value="office_2_district_1">Office 2 (district 1)</option>';
      html += '<option value="office_1_district_2">Office 1 (district 2)</option>';
      html += '<option value="office_2_district_2">Office 2 (district 2)</option>';
  }

  $('#edit-field-office').html(html);
  $('#edit-field-office').val(office_selected);

})(jQuery);
